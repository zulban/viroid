using UnityEngine;
using System.Collections;

public class VoxelData:MonoBehaviour
{	
	public int voxelCode = 0;
	public float strength = 1;
	public bool isRotationRandom=true;
	public Texture[] textures;
	public Texture[] alternateTextures;
}
