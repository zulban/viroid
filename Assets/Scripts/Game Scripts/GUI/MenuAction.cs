using UnityEngine;
using System.Collections;

public class MenuAction
{
	public string menuName;
	public string menuCommand;
	
	public MenuAction (string name, string command)
	{
		menuName = name;
		menuCommand = command;
	}
}
